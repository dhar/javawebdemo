package com.zyt.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdk.nashorn.internal.ir.debug.JSONWriter;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class TestServlet
 */
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static int count = 1;   
	
    public TestServlet() {
        super();
    }
    
    @Override
    protected void service(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException {
    	System.out.println("service");
    	super.service(arg0, arg1);
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doGet");
		
		// 处理Ajax跨域请求
		response.setHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS,DELETE");
		response.setHeader("Access-Control-Allow-Headers", "*");
		response.setHeader("Access-Control-Allow-Origin", "*");
		
//		response.getWriter().println("<html>");
//		response.getWriter().append("Served at: ").append(request.getContextPath()).append("<br>");
//		response.getWriter().println("getCookies: "+request.getCookies()+"<br/>");
//		response.getWriter().println("getMethod: "+request.getMethod()+"<br>");
//		response.getWriter().println("getQueryString: "+request.getQueryString()+"<br>");
//		response.getWriter().println("getRequestURI: "+request.getRequestURI()+"<br>");
//		response.getWriter().println("getRequestURL: "+request.getRequestURL()+"<br>");
//		response.getWriter().println("getServletPath: "+request.getServletPath()+"<br>");
//		response.getWriter().println("getSession: "+request.getSession()+"<br>");
//		response.getWriter().println("</html>");
		
		System.out.println("getCookies: "+request.getCookies()+"<br/>");
		System.out.println("getMethod: "+request.getMethod()+"<br>");
		System.out.println("getQueryString: "+request.getQueryString()+"<br>");
		System.out.println("getRequestURI: "+request.getRequestURI()+"<br>");
		System.out.println("getRequestURL: "+request.getRequestURL()+"<br>");
		System.out.println("getServletPath: "+request.getServletPath()+"<br>");
		System.out.println("getSession: "+request.getSession()+"<br>");
		System.out.println("getParameterMap: "+request.getParameterMap()+"<br>");
		
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("img", "https://gss0.bdstatic.com/-4o3dSag_xI4khGkpoWK1HF6hhy/baike/c0%3Dbaike80%2C5%2C5%2C80%2C26/sign=000fe204cbef7609280691cd4fb4c8a9/5366d0160924ab189bd007ae35fae6cd7a890b60.jpg");
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("error", 0);
		result.put("message", "阿斯顿发士大夫暗色阿斯蒂芬啊");
		result.put("data", dataMap);
		
//        JSONObject jo = JSONObject.fromObject(result);
//        System.out.println("result = " + jo);
		
		String imgUrl = "https://gss0.bdstatic.com/-4o3dSag_xI4khGkpoWK1HF6hhy/baike/c0%3Dbaike80%2C5%2C5%2C80%2C26/sign=000fe204cbef7609280691cd4fb4c8a9/5366d0160924ab189bd007ae35fae6cd7a890b60.jpg";
		String jsonString = "{\"error\":0, \"message\": \"YSK娃娃一只YSK娃娃一只YSK娃娃一只\", \"data\": {\"img\": \""+imgUrl+"\"}}";
		if (count++ % 2 == 0) {
			jsonString = "{\"error\":1, \"message\": \"\", \"data\": {\"img\": \""+imgUrl+"\"}}";
		} else {
			
		}
		System.out.println(jsonString);
		response.getWriter().println(jsonString);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doPost");
		doGet(request, response);
	}
	
	@Override
	protected void doOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doOptions");
		doGet(request, response);
	}
	
	@Override
	public ServletConfig getServletConfig() {
		return super.getServletConfig();
	}
	
	@Override
	public String getServletInfo() {
		return super.getServletInfo();
	}
	
	@Override
	public void init() throws ServletException {
		super.init();
		System.out.println("init");
	}
	
	@Override
	public void destroy() {
		super.destroy();
		System.out.println("destory");
	}

}
