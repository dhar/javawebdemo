package com.zyt.servlet.bookhandler;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zyt.util.MMLog;
import com.zyt.util.StringUtil;

public class UpdateBookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UpdateBookServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		response.getWriter().print("<html>");
		
		// 获取数据
		String bookCountStr = request.getParameter("bookCount");
		Integer bookCount = (!StringUtil.isEmptyString(bookCountStr)) ? Integer.parseInt(bookCountStr) : 0;
		String id = request.getParameter("id");
		String type = request.getParameter("type");

		// 数据库连接处理
		Connection conn = null;
		String url = "jdbc:mysql://localhost:3306/my-web-db?" + "useUnicode=true&characterEncoding=UTF8";
		String user = "zyt";
		String password = "1234";
		try {
			Class.forName("com.mysql.jdbc.Driver");

			conn = DriverManager.getConnection(url, user, password);
			Statement stmt = conn.createStatement();
			String sql = "CREATE TABLE IF NOT EXISTS book(id INTEGER, name VARCHAR(20), author VARCHAR(20), price INTEGER, bookCount INTEGER, PRIMARY KEY (id));";
			int result = stmt.executeUpdate(sql);
			if (result != -1) {
				// create table success
				MMLog.log("create table success");

				if (type != null && type.equalsIgnoreCase("delete")) {
					// 删除数据
					sql = "DELETE from book WHERE id=?";
					PreparedStatement pStatement = conn.prepareStatement(sql);
					pStatement.setString(1, id);
					int updateResult = pStatement.executeUpdate();
					if (updateResult > 0) {
						MMLog.log("删除成功");
						response.getWriter().println("更新成功");
					} else {
						MMLog.log("删除失败");
						response.getWriter().println("更新失败");
					}
					
				}  else {
					// 更新数据
					sql = "UPDATE book set bookCount=? WHERE id=?";
					PreparedStatement pStatement = conn.prepareStatement(sql);

					pStatement.setInt(1, bookCount);
					pStatement.setString(2, id);
					
					int updateResult = pStatement.executeUpdate();
					if (updateResult > 0) {
						MMLog.log("更新成功");
						response.getWriter().println("更新成功");
					}
					pStatement.close();
				}

				conn.close();
			}

		} catch (ClassNotFoundException | SQLException e) {
			MMLog.log("更新失败");
			response.getWriter().println("更新失败");
			e.printStackTrace();
		}
		
		response.getWriter().println("<a href=\"/JspJavaBeanDemo/servlets/BookListServlet\">返回</a>");
		response.getWriter().print("</html>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
