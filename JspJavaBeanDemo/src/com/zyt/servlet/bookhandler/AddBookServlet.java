package com.zyt.servlet.bookhandler;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zyt.model.Book;
import com.zyt.util.MMLog;
import com.zyt.util.StringUtil;

public class AddBookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AddBookServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		response.getWriter().print("<html>");
		response.getWriter().append("Served at: ").append(request.getContextPath());

		// 请求处理
		String name = request.getParameter("name");
		String author = request.getParameter("author");
		String priceStr = request.getParameter("price");
		double price = (!StringUtil.isEmptyString(priceStr)) ? Double.parseDouble(priceStr) : 0;
		String bookCountStr = request.getParameter("bookCount");
		Integer bookCount = (!StringUtil.isEmptyString(bookCountStr)) ? Integer.parseInt(bookCountStr) : 0;
		
		Book book = new Book();
		book.setName(name);
		book.setAuthor(author);
		book.setPrice(price);
		book.setBookCount(bookCount);
		
		if (!StringUtil.isEmptyString(book.getName())) {
			// 数据库连接处理
			Connection conn = null;
			String url = "jdbc:mysql://localhost:3306/my-web-db?"
					+ "useUnicode=true&characterEncoding=UTF8";
			String user = "zyt";
			String password = "1234";
			try {
				Class.forName("com.mysql.jdbc.Driver");

				conn = DriverManager.getConnection(url, user, password);
				Statement stmt = conn.createStatement();
				String sql = "CREATE TABLE IF NOT EXISTS book(id INTEGER, name VARCHAR(20), author VARCHAR(20), price INTEGER, bookCount INTEGER, PRIMARY KEY (id));";
				int result = stmt.executeUpdate(sql);
				if (result != -1) {
					// create table success
					MMLog.log("create table success");
					
					// 添加数据
					sql = "INSERT INTO book(name, author, price, bookCount) VALUES(?, ?, ?, ?)";
					PreparedStatement pstat = conn.prepareStatement(sql);
					pstat.setString(1, book.getName());
					pstat.setString(2, book.getAuthor());
					pstat.setDouble(3, book.getPrice());
					pstat.setInt(4, book.getBookCount());
					int row = pstat.executeUpdate();
					if (row > 0) {
						MMLog.log("添加成功");
						response.getWriter().println("添加成功");
					}
					
					pstat.close();
					conn.close();
				}

			} catch (ClassNotFoundException | SQLException e) {
				MMLog.log("添加失败");
				response.getWriter().println("添加失败");
				e.printStackTrace();
			}
		} else {
			MMLog.log("添加失败");
			response.getWriter().println("添加失败");
		}
	
		response.getWriter().println("<br />");
		response.getWriter().println("<a href=\"/JspJavaBeanDemo/add_book.jsp\">返回</a>");
		response.getWriter().print("</html>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
