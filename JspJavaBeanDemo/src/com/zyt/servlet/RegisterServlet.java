package com.zyt.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.zyt.util.MMLog;

/**
 * Servlet implementation class RegisterServlet
 */
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RegisterServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		
		// 处理注册 
		// response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		response.getWriter().print("<html>");
		response.getWriter().print("注册成功!<br>");
		response.getWriter().print("用户名："+ name+"<br/>");
		response.getWriter().print("密码："+ password+"<br/>");
		response.getWriter().print("</html>");
		
		MMLog.log("Test");;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
