package com.zyt.util;

public class StringUtil {
	public static boolean isEmptyString (String string) {
		return string == null || string.isEmpty();
	}
}
