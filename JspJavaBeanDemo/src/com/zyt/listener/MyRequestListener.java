package com.zyt.listener;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

public class MyRequestListener implements ServletRequestListener {

    public MyRequestListener() {
    }

    public void requestDestroyed(ServletRequestEvent arg0)  {
    	System.out.println("requestDestroyed");
    }

    public void requestInitialized(ServletRequestEvent arg0)  {
    	System.out.println("requestInitialized");
    }
	
}
