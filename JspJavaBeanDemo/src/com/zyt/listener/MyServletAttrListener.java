package com.zyt.listener;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;

public class MyServletAttrListener implements ServletContextAttributeListener {

    public MyServletAttrListener() {
    }

    public void attributeAdded(ServletContextAttributeEvent attributeEvent)  { 
    	String name = attributeEvent.getName();
    	Object value = attributeEvent.getValue();
    	
    	System.out.println("attributeAdded " + name + " : " + value);
    }

    public void attributeRemoved(ServletContextAttributeEvent arg0)  { 
    	System.out.println("attributeRemoved");
    }

    public void attributeReplaced(ServletContextAttributeEvent arg0)  { 
    	System.out.println("attributeReplaced");
    }
	
}
