package com.zyt.listener;

import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;

public class MyRequestAttrListener implements ServletRequestAttributeListener {

    public MyRequestAttrListener() {
    }

    public void attributeRemoved(ServletRequestAttributeEvent arg0)  { 
    	System.out.println("attributeRemoved");
    }

    public void attributeAdded(ServletRequestAttributeEvent arg0)  {
    	System.out.println("attributeAdded");
    }

    public void attributeReplaced(ServletRequestAttributeEvent arg0)  {
    	System.out.println("attributeReplaced");
    }
	
}
