package com.zyt.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MyServletContextListener implements ServletContextListener {

    public MyServletContextListener() {
    }

    public void contextDestroyed(ServletContextEvent arg0)  { 
    	System.out.println("contextDestroyed");
    }

    public void contextInitialized(ServletContextEvent arg0)  { 
    	System.out.println("contextInitialized");
    }
	
}
