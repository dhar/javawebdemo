<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String username = request.getParameter("username");
	System.out.println("username = "+username);
	// 跨域请求问题
	response.setHeader("Access-Control-Allow-Origin", "*");

	if(username != null) {
		if (username.startsWith("mr")) {
			out.print("恭喜，用户名可用");
		} else {
			out.print("抱歉，用户名不可用");
		}
	} else {
		out.print("用户名为空");
	}
%>