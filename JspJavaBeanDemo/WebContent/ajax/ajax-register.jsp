<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<style type="text/css">
	
</style>

<script type="text/javascript">
	var httpRequest;
	
	function createRequest(url) {
		httpRequest = null;
		if(window.XMLHttpRequest){
			httpRequest = new XMLHttpRequest();
		} else if(window.ActiveXObject){
			try {
				httpRequest = new ActiveXObject("Msxml.XMLHTTP");
			} catch (e) {
				try {
					httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
				}
			}
		}
		
		if (!httpRequest) {
			alert("can not create XMLHttpRequest object");
			return false;
		}
		
		httpRequest.onreadystatechange = getResult;
		httpRequest.open("GET", url, true);
		httpRequest.send(null);
		
		return httpRequest;
	}
	
	function getResult() {
		if (httpRequest.readyState == 4) {
			console.log("statue = ", httpRequest.status);
			console.log("httpRequest.status == 200 >>", httpRequest.status == 200);
			var statusResult = httpRequest.status == 200;
			if (statusResult) {
				var tipsElement = document.getElementById("tips");
				tipsElement.innerHTML = httpRequest.responseText;
				console.log("httpRequest.responseText = ", httpRequest.responseText);
				console.log("document.getElementById(\"tips\") = ", document.getElementById("tips"));
				alert(httpRequest.responseText);
				document.getElementById("tips").style.display = "block";
			} else {
				// alert("请求错误");
			}
		}
	}

	function checkUserName(username) {
		createRequest("checkUser.jsp?username="+username.value);
	}
	
	
	function changeTips() {
		document.getElementById("tips").innerHTML = "Tips Changed";
	}
	
	function name() {
		
	}
	
</script>
</head>
<body>
	<form action="" name="form1">
		<input type="text" name="username"><button onclick="checkUserName(form1.username)">检测用户名</button>
		<div id="tips" style="color: red">用户名不可用</div>
		<br>
		<input type="submit" value="Register">

		<br>		
	</form>
	
	<input type="text" onclick="changeTips()" value="Change Tips">
</body>
</html>