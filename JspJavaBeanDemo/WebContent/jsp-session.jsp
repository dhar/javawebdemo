<%@page import="java.util.Enumeration"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		session.setAttribute("username", "zyt");
		session.setAttribute("card", "11");
		
		session.setAttribute("tmp", "tmpValue");
		session.removeAttribute("tmp");

	%>
	Sessions:
	<br>
	<%
		Enumeration<String> attrNames = session.getAttributeNames();
		while (attrNames.hasMoreElements()) {
			String element = attrNames.nextElement();
			if (null != element) {
				Object headerValue = session.getAttribute(element);
				out.println(
						"&nbsp;&nbsp;" + element + " : " + (headerValue != null ? headerValue : "null") + "<br/>");
			}
		}
		;
	%>
	
	<br>
	getLastAccessedTime:<%=session.getLastAccessedTime() %>s
	<br>
	getMaxInactiveInterval:<%=session.getMaxInactiveInterval() %>s
	<br>
	getMaxInactiveInterval:<%=session.getMaxInactiveInterval() %>s
</body>
</html>