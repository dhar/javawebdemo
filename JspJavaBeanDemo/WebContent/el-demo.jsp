<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>


	<%
		session.setAttribute("username", "angle baby");
	%>
	访问session中的username属性： ${username}<br>

	访问数组：<br>
	<%
		String[] arr = {"aaaaa", "bbbb", "ccccc"};
		session.setAttribute("stringArr", arr);
		for(int i = 0; i<arr.length; i++) {
			// EL 表达式不能直接访问i,保存到request容器中间接访问
			request.setAttribute("index", i);
		%>
		${index}：${stringArr[index]}<br/>
		<%
		}
	%>
	
	<br>
	EL运算符操作：<br>
	${1+23}
	
	<br>
	EL empty/not empty <br>
	<%request.setAttribute("user", "aa"); %>
	<%request.setAttribute("user1", null); %>
	<%request.setAttribute("user2", ""); %>

	is user empty: ${empty user} <br>
	is user1 empty: ${empty user1} <br>
	is user2 empty: ${empty user2} <br>
	
	is user2 not empty: ${not empty user2} <br>
	
	<br>
	EL 条件表达式：<br>
	is user2 empty: ${empty user2 ? "user2 为空" : "user2 不为空"} <br>
	
	<br>
	EL pageContext:<br>
	request.serverName:${pageContext.request.serverName }<br>
	request.serverPort:${pageContext.request.serverPort }<br>
	request.servletPath:${pageContext.request.servletPath }<br>
	
	response.contentType:${pageContext.response.contentType }<br>
	response.characterEncoding:${pageContext.response.characterEncoding }<br>
	
	<br>
	EL Scope: page/request/session/application:<br>
	<jsp:useBean id="book" class="com.zyt.model.Book" scope="page">
	</jsp:useBean>
	<jsp:setProperty property="name" name="book" value="this is book name"/>
	
	pageScope.book.name： ${pageScope.book.name}<br>
	
</body>
</html>