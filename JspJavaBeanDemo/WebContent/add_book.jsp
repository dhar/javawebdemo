<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form
		action="http://localhost:8080/JspJavaBeanDemo/servlets/AddBookServlet"
		method="get">
		<table align="center" width="500" border="1" style="border-collapse: 0; border-spacing: 0">
			<tr>
				<td align="center" colspan="2">添加图书信息
					<hr />
				</td>
			</tr>
			<tr>
				<td align="right">书名：</td>
				<td><input name="name" type="text"></td>
			</tr>
			<tr>
				<td align="right">作者：</td>
				<td><input name="author" type="text"></td>
			</tr>
			<tr>
				<td align="right">价格：</td>
				<td><input name="price" type="text"></td>
			</tr>
			<tr>
				<td align="right">数量：</td>
				<td><input name="bookCount" type="text"></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center;">
					<input type="submit" value="确定" style="width: 100px; height: 60px">
				</td>
			</tr>
		</table>
		
	</form>
</body>
</html>