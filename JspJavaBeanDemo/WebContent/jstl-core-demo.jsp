<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	c:out 输出标签:<br>
	<c:out value="分割线 escapeXml=true <hr>" escapeXml="true"></c:out><br>
	<c:out value="分割线 escapeXml=false <hr>" escapeXml="false"></c:out>
	
	<br>
	<% request.setAttribute("rObj", "rObjValue"); %>
	c:set 变量设置标签:<br>
	<c:set var="rObj" value="rObjJSTLSettledValue" scope="request"></c:set><br>
	request.getAttribute("rObj"): <%=request.getAttribute("rObj") %><br>
	requestScope.rObj: ${requestScope.rObj}<br>
	
	<br>
	<jsp:useBean id="book" class="com.zyt.model.Book" scope="request">
	</jsp:useBean>
	
	//  target="${book}  target 使用EL表达式传值<br>
	<c:set target="${book}" value="rObjJSTLSettledBookName" property="name"></c:set>
	request.getAttribute("book"): <%=request.getAttribute("book") %><br>
	requestScope.rObj: ${requestScope.book}<br>
	
	<c:set target="${book}" property="author">rObjJSTLSettledBookAuthor</c:set>
	request.getAttribute("book"): <%=request.getAttribute("book") %><br>
	requestScope.rObj: ${requestScope.book}<br>
	
	<br>
	c:remove<br>
	<c:remove var="book" scope="request"/>
	request.getAttribute("book"): <%=request.getAttribute("book") %><br>
	requestScope.rObj: ${requestScope.book}<br>
	
	<br>
	c:catch<br>
	<c:catch var="error">
		<!-- code might raise exception -->
		<jsp:useBean id="catchBook" class="com.zyt.model.Book"></jsp:useBean>
		<c:set target="${catchBook}" property="nonexistsProperty">value</c:set>
	</c:catch>
	<c:out value="${error }"></c:out>
	
	<br>
	c:import<br>
	<c:import url="/register.jsp"></c:import>
	
	<br>
	c:url<br>
	<c:url var="path" value="register.jsp" scope="page">
		<c:param name="user" value="mr"></c:param>
		<c:param name="email" value="mremail@example.com"></c:param>
	</c:url>
	<a href="${pageScope.path }">register</a><br>
	
	<br>
	c:redirect<br>
	<%-- <c:redirect url="/register.jsp"></c:redirect> --%>
	
	<br>
	c:if<br>
	<c:if test="false">
		c:if
	</c:if>
	
	<br>
<%-- 	c:choose<br>
	<c:choose>
		
	</c:choose> --%>
	
	
	<br>
	<%
		ArrayList<String> list = new ArrayList<String>();
		list.add("aaaaaa");
		list.add("bbbbbb");
		list.add("cccccc");
		request.setAttribute("list", list);
	%>
	c:forEach<br>
	<c:forEach items="${requestScope.list }" var="itm" varStatus="st">
	 	${st.index } : ${itm }
	</c:forEach>
	
	<br>
	c:forTokens<br>
	<c:forTokens items="asdf,123,123,4534,zsdf,yasreta,asdf" delims="," var="itm" varStatus="st">
		${st.index } : ${itm }
	</c:forTokens>
	
</body>
</html>