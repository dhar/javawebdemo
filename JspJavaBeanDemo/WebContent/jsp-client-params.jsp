<%@page import="java.util.Enumeration"%>
<%@page import="sun.reflect.ReflectionFactory.GetReflectionFactoryAction"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<br>
	getMethod:<%=request.getMethod() %>
	<br>
	getProtocol:<%=request.getProtocol() %>
	<br>
	getRequestURI:<%=request.getRequestURI() %>
	<br>
	getRequestURL:<%=request.getRequestURL() %>
	<br>
	getServerName:<%=request.getServerName() %>
	<br>
	getRemoteAddr:<%=request.getRemoteAddr() %>
	<br>
	getRemoteHost:<%=request.getRemoteHost() %>
	<br>
	getServletPath:<%=request.getServletPath() %>
	<br>
	getServerPort:<%=request.getServerPort() %>
	<br>
	<%!
		static int count = 0;
	%>
	getHeaderNames:<br>
	<%
	Enumeration<String> headerNames = request.getHeaderNames();
	 while(headerNames.hasMoreElements()) {
		 String element = headerNames.nextElement();
		 if (null != element) {
			 String headerValue = request.getHeader(element);
			 out.println("&nbsp;&nbsp;"+element+" : "+(headerValue!=null?headerValue:"null")+"<br/>");
		 }
		 count++;
		 if(count==100){
			 break;
		 }
	 }; 
	%>
</body>
</html>