<%@page import="java.util.ArrayList"%>
<%@page import="com.zyt.model.Book"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<table align="center" width="500" border="1"
		style="border-spacing: 0; border-collapse: 0">
		<tr>
			<td align="center" colspan="7">
				<h2>所有图书信息</h2>
				<hr />
			</td>
		</tr>
		<tr>
			<td>ID</td>
			<td>name</td>
			<td>price</td>
			<td>count</td>
			<td>author</td>
			<td>update count</td>
			<td>delete</td>
		</tr>

		<%
			ArrayList<Book> books = (ArrayList<Book>) request.getAttribute("books");
			if (books != null) {
				for (int i = 0; i < books.size(); i++) {
					Book book = books.get(i);
		%>

		<tr>
			<td><%=book.getId()%></td>
			<td><%=book.getName()%></td>
			<td><%=book.getPrice()%></td>
			<td><%=book.getBookCount()%></td>
			<td><%=book.getAuthor()%></td>
			<td>
				<form
					action="http://localhost:8080/JspJavaBeanDemo/servlets/UpdateBookServlet">
					<input type="hidden" name="id" value="<%=book.getId()%>"> <input
						type="text" name="bookCount"> <input type="submit"
						value="Update">
				</form>
			</td>
			<td>
			<a href="UpdateBookServlet?id=<%=book.getId()%>&type=delete">删除</a>
			</td>
		</tr>

		<%
			}
			} else {
				out.print("没有数据");
			}
		%>

	</table>
</body>
</html>
