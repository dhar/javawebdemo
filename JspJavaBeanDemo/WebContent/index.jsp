<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	Hello world
	<br />
	<!-- JSP中内嵌Java代码 -->
	现在时间：<%
		out.print(new java.util.Date());
	%>
	<br>

	<%!
	// 声明一个整形变量
	public int count;
	public int count2;

	public String info() {
		return "Hello";
	}%>

	<%
		out.print(info());
	%>

	<!-- 输出JSP表达式 -->
	<%
		out.print(count++);
	%>
	<br>
	<%=count2++%>

	<table style="border: 1px solid red; width: 300px">
		<!-- JSP 脚本 -->
		<%
			for (int i = 0; i < 10; i++) {
		%>
		<tr>
			<td>循环值：</td>
			<td><%=i%></td>
		</tr>
		<%
			}
		%>
	</table>
</body>
</html>