<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

值栈：<br>
Student Id: <s:property value="student.id"/><br>
Student Name: <s:property value="student.name"/><br>
Student say(): <s:property value="student.say()"/><br>

<br>	
Servelt Scope Object:<br>
#session.sessionKey: <s:property value="#session.sessionKey"/><br>
#session['sessionKey']: <s:property value="#session['sessionKey']"/><br>

<br>	
容器Map：<br>
Scheme:<s:property value="map.scheme"/><br>
IP:<s:property value="map.ip"/><br>
PORT:<s:property value="map.port"/><br>

<br>	
容器Array：<br>
array.size():<s:property value="array.size()"/><br>
array[0]:<s:property value="array[0]"/><br>

<br>
<s:a href="#" action="UserAction">Struts a</s:a><br>

</body>
</html>