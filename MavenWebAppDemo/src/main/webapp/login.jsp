<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<s:form action="UserAction!login">
		<s:textfield name="username" label="User Name" required="true" requiredposition="right"></s:textfield>
		<br>
		<s:password name="password" label="Password" required="true" requiredposition="right"></s:password>
		<br>
		<s:submit value="Login"></s:submit>
	</s:form>
</body>
</html>