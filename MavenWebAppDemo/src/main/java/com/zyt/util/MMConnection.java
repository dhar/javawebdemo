package com.zyt.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MMConnection {
	
	private static final String url = "jdbc:mysql://localhost:3306/my-web-db?" + "useUnicode=true&characterEncoding=UTF8";
	private static final String user = "zyt";
	private static final String password = "1234";
	
	public static Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}
}
