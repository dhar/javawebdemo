package com.zyt.action;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.zyt.util.MMLog;

public class UserAction extends ActionSupport {
	private String username;
	private String password;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String execute() throws Exception {
		return SUCCESS;
	}
	
	public String login(){
		MMLog.log("===");
		// struts 获取 servlet 的域对象（request/page/session/application）
		Map<String, String> requestMap = (Map<String, String>) ActionContext.getContext().get("request");
		String userName = requestMap.get("username");
		String password = requestMap.get("password");
		this.username = userName;
		this.password = password;
		return SUCCESS;
	}
	
	public String logout(){
		return "logout";
	}
}
