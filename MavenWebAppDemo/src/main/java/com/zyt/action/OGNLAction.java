package com.zyt.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.zyt.model.Student;

public class OGNLAction extends ActionSupport {
	
	private Student student = new Student();
	private HashMap<String, String> map = new HashMap<String, String>();
	private ArrayList<String> array = new ArrayList<String>();

	public Student getStudent() {
		return student;
	}
	
	public void setStudent(Student student) {
		this.student = student;
	}
	
	public static String staticFunc() {
		return "Static func call";
	}
	
	public Map<String, String> getMap() {
		return map;
	}
	
	public ArrayList<String> getArray() {
		return array;
	}
	
	@Override
	public String execute() throws Exception {

		// 设置servlet域对象
		Map<String, Object> sessionMap = ActionContext.getContext().getSession();
		sessionMap.put("sessionKey", "SessionValue");
		
		// 设置值栈数据		
		student.setName("黄晓明");
		student.setId(123);
		
		// 容器内
		array.add("item1");
		array.add("item1");
		
		map.put("ip", "123.123.123.123");
		map.put("port", "21");
		map.put("scheme", "ftp");

		
		return SUCCESS;
	}
}
