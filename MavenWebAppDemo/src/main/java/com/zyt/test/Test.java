package com.zyt.test;

import java.io.UnsupportedEncodingException;
import java.util.Random;

import org.hibernate.Session;

import com.zyt.model.Factory;
import com.zyt.model.Product;
import com.zyt.model.Student;
import com.zyt.util.MMHibernateUtil;

public class Test {
	public static void main(String[] args) {
		
		// POJO
		/*
		// 添加数据
		Student student = new Student();
		student.setName("张良");
		
		Session session = MMHibernateUtil.getSession();
		session.beginTransaction();
		session.save(student);
		session.getTransaction().commit();
		*/
		
		/*
		// 获取数据
		Session session = MMHibernateUtil.getSession();
		Student student = session.get(Student.class, 1);
		System.out.println("Student name:  " + student.getName());
		
		student = session.get(Student.class, 2);
		if (student != null) {
			 System.out.println("Student name:  " + student.getName());
		}
		*/
		
		/*
		// 删除数据
		Session session = MMHibernateUtil.getSession();
		Student student = session.get(Student.class, 1);
		if (student != null) {
			session.beginTransaction();
			session.delete(student);
			session.getTransaction().commit();
		}
		*/
		
		/*
		// 数据数据入库
		Session session = MMHibernateUtil.getSession();
		session.beginTransaction();
		for (int i = 0; i < 20; i++) {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(getRandomChar());
			stringBuilder.append(getRandomChar());
			Student student = new Student();
			student.setName(stringBuilder.toString());
			session.save(student);
		}
		session.getTransaction().commit();
		*/
		
		/*
		Session session = MMHibernateUtil.getSession();
		session.beginTransaction();
		for (int i = 0; i < 2; i++) {
			Product product = new Product();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(getRandomChar());
			stringBuilder.append(getRandomChar());
			product.setName(stringBuilder.toString());
			product.setPrice(123);
			
			Factory factory = new Factory();
			stringBuilder = new StringBuilder();
			stringBuilder.append(getRandomChar());
			stringBuilder.append(getRandomChar());
			factory.setName(stringBuilder.toString());
			product.setFactory(factory);
			
			session.save(product);
		}
		session.getTransaction().commit();
		 */
		
		/*
		// 多对一的单向测试代码
		Session session = MMHibernateUtil.getSession();
		session.beginTransaction();
		Product product = session.get(Product.class, 1);
		session.getTransaction().commit();
		if (product != null) {
			System.out.println("product getName:  " + product.getName());
			System.out.println("product getFactory getName:  " + product.getFactory().getName());
		} else {
			System.out.println("product is null");
		}
		*/
		
		/*
		// 多对一的双向测试代码
		Session session = MMHibernateUtil.getSession();
		session.beginTransaction();
		Factory factory = session.get(Factory.class, 1);
		session.getTransaction().commit();
		if (factory != null) {
			System.out.println("factory getName:  " + factory.getName());
			 System.out.println("factory.getProducts().size():  " + factory.getProducts().size());
		} else {
			System.out.println("product is null");
		}
		*/
		
		
		// 实体关系继承映射
		Student student = new Student();
		student.setName("高晓松");
		student.setAge(46);
		student.setGender(1);
		student.setSchool("清华大学");
		Session session = MMHibernateUtil.getSession();
		session.beginTransaction();
		session.save(student);
		session.getTransaction().commit();
		
	}
	
	private static char getRandomChar() {
        String str = "";
        int hightPos; //
        int lowPos;

        Random random = new Random();

        hightPos = (176 + Math.abs(random.nextInt(39)));
        lowPos = (161 + Math.abs(random.nextInt(93)));

        byte[] b = new byte[2];
        b[0] = (Integer.valueOf(hightPos)).byteValue();
        b[1] = (Integer.valueOf(lowPos)).byteValue();

        try {
            str = new String(b, "GBK");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            System.out.println("错误");
        }

        return str.charAt(0);
    }
}
