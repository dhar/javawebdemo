package com.zyt.listener;

import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionEvent;

public class MySessionActivListener implements HttpSessionActivationListener {

	public MySessionActivListener() {
	}

	public void sessionDidActivate(HttpSessionEvent arg0) {
		System.out.println("sessionDidActivate");
	}

	public void sessionWillPassivate(HttpSessionEvent arg0) {
		System.out.println("sessionWillPassivate");
	}

}
