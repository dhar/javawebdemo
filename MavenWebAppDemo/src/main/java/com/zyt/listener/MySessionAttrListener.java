package com.zyt.listener;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

public class MySessionAttrListener implements HttpSessionAttributeListener {

    public MySessionAttrListener() {
    }

    public void attributeAdded(HttpSessionBindingEvent arg0)  { 
    	System.out.println("attributeAdded");
    }

    public void attributeRemoved(HttpSessionBindingEvent arg0)  {
    	System.out.println("attributeRemoved");
    }

    public void attributeReplaced(HttpSessionBindingEvent arg0)  {
    	System.out.println("attributeReplaced");
    }
}
