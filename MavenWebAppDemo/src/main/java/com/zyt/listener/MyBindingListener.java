package com.zyt.listener;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

public class MyBindingListener implements HttpSessionBindingListener {

    public MyBindingListener() {
    }

    public void valueBound(HttpSessionBindingEvent arg0)  {
    	System.out.println("valueBound");
    }

    public void valueUnbound(HttpSessionBindingEvent arg0)  {
    	System.out.println("valueUnbound");
    }
	
}
