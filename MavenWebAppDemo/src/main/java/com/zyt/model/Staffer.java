package com.zyt.model;

public class Staffer extends Person {
	private String company;
	
	public String getCompany() {
		return company;
	}
	
	public void setCompany(String company) {
		this.company = company;
	};
}
