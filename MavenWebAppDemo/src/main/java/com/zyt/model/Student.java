package com.zyt.model;

public class Student extends Person{
	
	private String school;
	
	public String getSchool() {
		return school;
	}
	
	public void setSchool(String school) {
		this.school = school;
	}
	
	public String say() {
		return "My Name is "+this.getName();
	}
}
