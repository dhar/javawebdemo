DROP TABLE IF EXISTS student

CREATE TABLE IF NOT EXISTS tb_student(
	id INTEGER NOT NULL AUTO_INCREMENT,
	name VARCHAR(20),
	PRIMARY KEY (id)
);

DROP TABLE IF EXISTS t_product;
CREATE TABLE IF NOT EXISTS t_product(
	id INTEGER AUTO_INCREMENT,
	name TEXT,
	price INTEGER,
	factory_id INTEGER,
	PRIMARY KEY(id),
	CONSTRAINT `t_product` FOREIGN KEY (`factory_id`) REFERENCES `t_factory` (`id`)
);

CREATE TABLE IF NOT EXISTS t_factory(
	id INTEGER AUTO_INCREMENT,
	name TEXT,
	PRIMARY KEY(id)
);

insert into t_product (name, price, factory_id) values ('鱼羊野史', 49, 1);
insert into t_product (name, price, factory_id) values ('鱼羊野史2', 49, 1);
insert into t_product (name, price, factory_id) values ('鱼羊野史2', 49, 1);
insert into t_factory (name) values ('民族建设出版社');

select * from t_product;
